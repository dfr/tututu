
var gpxUtil = (function() {

    var GPX_NS = "http://www.topografix.com/GPX/1/1";
    var XSI_NS = "http://www.w3.org/2001/XMLSchema-instance";

    function parse_gpx(input) {
        var gpx = parse_xml_str(input);
        return parse_gpx_xml_data(gpx);
    }

    function parse_gpx_xml_data(xml) {
        var gpx = { tracks: [], wpts: [], name: null, desc: null };
        var j, i, el = [];

        // meta
        var meta = ['name','desc','author','copyright'];
        for(i = 0; i < meta.length; i++) {
            var v = xml.getElementsByTagName(meta[i]);
            if (v.length > 0) gpx[meta[i]] = v[0].textContent;
        }

        // wpt
        el = xml.getElementsByTagName('wpt');
        for (i = 0; i < el.length; i++) {
            var wpt = parse_wpt(el[i]);
            gpx.wpts.push(wpt);
        }

        // tags
        var tags = [['rte','rtept'], ['trkseg','trkpt']];
        for (j = 0; j < tags.length; j++) {
            el = xml.getElementsByTagName(tags[j][0]);
            for (i = 0; i < el.length; i++) {
                var coords = parse_trkseg(el[i], tags[j][1]);
                if (coords.length === 0) continue;
                gpx.tracks.push(coords);
            }
        }

        return gpx;
    }

    function parse_wpt(line) {
        var coords = { lat: line.getAttribute('lat'), lng: line.getAttribute('lon') };
        var wpt = { coords: coords };
        var wpt_tags = ['ele','name','sym','type'];
        for(var i = 0; i < wpt_tags.length; i++) {
            var v = line.getElementsByTagName(wpt_tags[i]);
            if (v.length > 0) wpt[wpt_tags[i]] = v[0].textContent;
        }

        return wpt;
    }

    function parse_trkseg(line, tag) {
        var el = line.getElementsByTagName(tag);
        if (!el.length) return [];
        var coords = [];
        var last = null;

        for (var i = 0; i < el.length; i++) {
            var _, ll = { lat: el[i].getAttribute('lat'), lng: el[i].getAttribute('lon') };
            ll.meta = { time: null, ele: null, hr: null };
            coords.push(ll);
        }

        return coords;
    }

    function parse_xml_str(input) {
        var xmlDoc;
        if (window.DOMParser)  {
            var parser=new DOMParser();
            xmlDoc=parser.parseFromString(input,"text/xml");
        }
        else { // Internet Explorer
            xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
            xmlDoc.async=false;
            xmlDoc.loadXML(input);
        }
        return xmlDoc;
    }

    function stringify(gpxData) {
        var gpxNode = document.createElementNS(GPX_NS, "gpx");
        var i, len;
        gpxNode.setAttribute("version", "1.1");
        gpxNode.setAttribute("creator", gpxData.author);
        gpxNode.setAttribute("xmlns:xsi", XSI_NS);
        gpxNode.setAttribute("xsi:schemaLocation", "http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd");
        gpxNode.appendChild(build_metadata_node(gpxData));
        for(i=0, len=gpxData.wpts.length; i<len; i++) {
            gpxNode.appendChild(build_wpt_node(gpxData.wpts[i]));
        }
        for(i=0, len=gpxData.tracks.length; i<len; i++) {
            gpxNode.appendChild(build_feature_node(gpxData.tracks[i]));
        }
        var serializer = new XMLSerializer();
        return serializer.serializeToString(gpxNode);
    }

    function build_metadata_node(metadata) {
        var types = ['name', 'desc', 'author'],
            node = document.createElementNS(GPX_NS, 'metadata');
        for (var i=0; i < types.length; i++) {
            var type = types[i];
            if (metadata[type]) {
                var n = document.createElementNS(GPX_NS, type);
                n.appendChild(document.createTextNode(metadata[type]));
                node.appendChild(n);
            }
        }
        return node;
    }

    function build_feature_node(points) {
        var trkNode = document.createElementNS(GPX_NS, "trk");
        trkNode.appendChild(build_trkseg_node(points));
        return trkNode;
    }

    function build_trkseg_node(geometry) {
        var node, i, len;
        node = document.createElementNS(GPX_NS, "trkseg");
        for (i = 0, len=geometry.length; i < len; i++) {
            node.appendChild(build_trkpt_node(geometry[i]));
        }
        return node;
    }

    function build_trkpt_node(point) {
        var node = document.createElementNS(GPX_NS, "trkpt");
        node.setAttribute("lon", point.lng);
        node.setAttribute("lat", point.lat);
        return node;
    }

    function build_wpt_node(wpt) {
        var types = ['ele','name','sym','type'];
        var node = document.createElementNS(GPX_NS, "wpt");
        node.setAttribute("lon", wpt.coords.lng);
        node.setAttribute("lat", wpt.coords.lat);
        for (var i=0; i < types.length; i++) {
            var type = types[i];
            if (wpt[type]) {
                var n = document.createElementNS(GPX_NS, type);
                n.appendChild(document.createTextNode(wpt[type]));
                node.appendChild(n);
            }
        }
        return node;
    }


    return { parse: parse_gpx, stringify: stringify };

})();

