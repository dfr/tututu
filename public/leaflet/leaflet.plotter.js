L.Polyline.plotter = L.Polyline.extend({
    _lineMarkers: [],
    _editIcon: L.divIcon({ iconSize: [10, 10], className: 'leaflet-div-icon leaflet-editing-icon'}),
    _editStartIcon: L.divIcon({ iconSize: [10, 10], className: 'leaflet-div-icon leaflet-editing-icon checkers-bg'}),
    _editNoteIcon: L.divIcon({ iconSize: [10, 10], className: 'leaflet-div-icon leaflet-editing-icon note-icon'}),
    _halfwayPointMarkers: [],
    _existingLatLngs: [],
    options: {
        weight: 2,
        color: '#000',
        readOnly: false
    },
    initialize: function (latlngs, options){
        this._existingLatLngs = latlngs;
        L.Polyline.prototype.initialize.call(this, [], options);
    },
    onAdd: function (map) {
        L.Polyline.prototype.onAdd.call(this, map);
        this._map = map;
        this._plotExisting();
        if(!this.options.readOnly){
            this._bindMapClick();
        }
    },
    onRemove: function(){
        for(var index in this._halfwayPointMarkers){
            this._map.removeLayer(this._halfwayPointMarkers[index]);
        }
        for(index in this._lineMarkers){
            this._map.removeLayer(this._lineMarkers[index]);
        }
        this._halfwayPointMarkers = this._lineMarkers = [];
        this._unbindMapClick();
        L.Polyline.prototype.onRemove.call(this, map);
    },
    removeAllPoints: function(e){
        for(var i = 0; i < this._lineMarkers.length; i++) {
            this._map.removeLayer(this._lineMarkers[i]);
        }
        this._lineMarkers = [];
        this._replot();
        this.fireEvent('plotter.rm', e);
    },
    reversePoints: function(e){
        this._lineMarkers[0].setIcon(this._editIcon);
        this._lineMarkers.reverse();
        this._lineMarkers[0].setIcon(this._editStartIcon);
    },
    setNewPoints: function(points, wpts){
        // wpts hash
        var wpth = {};
        for(var i = 0; i < wpts.length; i++) {
            var k = '' + wpts[i].coords.lat + '-' + wpts[i].coords.lng;
            wpth[k] = wpts[i];
        }
        // remove old
        for(i = 0; i < this._lineMarkers.length; i++) {
            this._map.removeLayer(this._lineMarkers[i]);
        }
        this._lineMarkers = [];
        // add fresh
        for(i = 0; i < points.length; i++) {
            k = '' + points[i].lat + '-' + points[i].lng;
            var note = (k in wpth) ? wpth[k].name : null;
            this._addNewMarker({ latlng: points[i] }, note);
        }
        this._replot();
        this.fireEvent('plotter.alter');
    },
    getWpts: function() {
        var wpts = [];
        for(var i = 0; i < this._lineMarkers.length; i++) {
            if('znote' in this._lineMarkers[i] && this._lineMarkers[i].znote)
                wpts.push({ coords: this._lineMarkers[i].getLatLng(), name: this._lineMarkers[i].znote });
        }
        return wpts;
    },
    setLatLngs: function(latlngs){
        L.Polyline.prototype.setLatLngs.call(this, latlngs);
    },
    _bindMapClick: function(){
        this._map.on('click', this._onMapClick, this);
    },
    _unbindMapClick: function(){
        this._map.off('click', this._onMapClick, this);
    },
    _replot: function(){
        this._redraw();
        this._redrawHalfwayPoints();
    },
    _unbindMarkerEvents: function(marker){
        //marker.off('click', this._removePoint, this);
        marker.off('drag', this._replot, this);
        marker.off('dragend');
        marker.dragging.disable();
    },
    _bindMarkerEvents: function(marker){
        //marker.on('click', this._removePoint, this);
        marker.on('drag', this._replot, this);
        marker.on('dragend', function(e) { this.fireEvent('plotter.alter', e); }, this);
        marker.dragging.enable();
    },
    _bindHalfwayMarker: function(marker){
        marker.on('click', this._addHalfwayPoint, this);
    },
    _unbindHalfwayMarker: function(marker){
        marker.off('click', this._addHalfwayPoint, this);
    },
    _addToMapAndBindMarker: function(newMarker){
        //var id = L.stamp(newMarker);

        // make popup
        var popup_div = make_marker_popup(this, newMarker);
        newMarker.bindPopup(popup_div[0]);

        newMarker.addTo(this._map);
        if(!this.options.readOnly){
            this._bindMarkerEvents(newMarker);
        }
    },
    removePoint: function(obj){
        var targetIdx = this._lineMarkers.indexOf(obj);
        if(targetIdx == 0 && this._lineMarkers.length > 1) {
            this._lineMarkers[1].setIcon(this._editStartIcon);
        }
        this._map.removeLayer(obj);
        this._lineMarkers.splice(targetIdx, 1);
        this._replot();
        this.fireEvent('plotter.rm');
    },
    _onMapClick: function(e){
        this._addNewMarker(e);
        this._replot();
        this.fireEvent('plotter.add', e);
    },
    _addNewMarker: function(e, note){
        var icon = this._lineMarkers.length == 0 ? this._editStartIcon : note ? this._editNoteIcon : this._editIcon;
        var newMarker = new L.marker(e.latlng, { icon: icon });
        if(note) newMarker.znote = note;
        this._addToMapAndBindMarker(newMarker);
        this._lineMarkers.push(newMarker);
    },
    _redrawHalfwayPoints: function(){
        for(var index in this._halfwayPointMarkers){
            this._map.removeLayer(this._halfwayPointMarkers[index]);
        }
        this._halfwayPointMarkers = [];
        for(index in this._lineMarkers){
            index = parseInt(index);
            if(typeof this._lineMarkers[index + 1] === 'undefined'){
                return;
            }
            var halfwayMarker = new L.Marker([
                (this._lineMarkers[index].getLatLng().lat + this._lineMarkers[index + 1].getLatLng().lat) / 2,
                (this._lineMarkers[index].getLatLng().lng + this._lineMarkers[index + 1].getLatLng().lng) / 2,
            ], { icon: this._editIcon, opacity: 0.5 }).addTo(this._map);
            halfwayMarker.index = index;
            if(!this.options.readOnly){
                this._bindHalfwayMarker(halfwayMarker);
            }
            this._halfwayPointMarkers.push(halfwayMarker);
        }
    },
    _addHalfwayPoint: function(e){
        var newMarker = new L.marker(e.latlng, { icon: this._editIcon });
        this._addToMapAndBindMarker(newMarker);
        this._lineMarkers.splice(e.target.index + 1, 0, newMarker);
        this._replot();
        this.fireEvent('plotter.add', e);
    },
    _plotExisting: function(){
        for(var index in this._existingLatLngs){
            this._addNewMarker({
                latlng: new L.LatLng(
                    this._existingLatLngs[index][0],
                    this._existingLatLngs[index][1]
                )
            });
        }
	    this._replot();
    },
    _redraw: function(){
        this.setLatLngs([]);
        this.redraw();
        for(var index in this._lineMarkers){
            this.addLatLng(this._lineMarkers[index].getLatLng());
        }
        this.redraw();
    }
});

function make_marker_popup(self, newMarker) {
    var div = $('<div/>');
    var a_rm = $('<a href="#" class="rm-point" style="color:red; text-decoration:none">удалить точку</a>');
    a_rm.click(function() {
        self.removePoint(newMarker);
    });
    // заметка
    if(self._lineMarkers.length > 0) {
        div.append($('<b>Заметка</b>'));
        div.append($('<br/>'));
        var inp_note = $('<input type="text" size="22" />');
        if(newMarker.znote) inp_note.val(newMarker.znote);
        div.append(inp_note);
        div.append($('<br/>'));
        var btn_save = $('<button type="button">Сохранить</button>');
        btn_save.click(function() {
            if(inp_note.val())
                newMarker.setIcon(self._editNoteIcon);
            else
                newMarker.setIcon(self._editIcon);
            newMarker.znote = inp_note.val();
            newMarker.closePopup();
        });
        div.append(btn_save);
        div.append($('<br/>'));
    }
    
    div.append(a_rm);
    div.append($('<br/>'));
    return div;
}

L.Polyline.Plotter = function(latlngs, options){
    return new L.Polyline.plotter(latlngs, options);
};
