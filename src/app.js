const DEF_TRACK_OPACITY = 0.35;
const DEF_MAPIA_OPACITY = 0.35;
const FILE_API_SUPPORTED = window.File && window.FileReader;
const WIKIMAPIA_KEY = '41F4981A-1BD068F3-770D849B-345A4C4B-496E3A04-CF0F0ABF-44F459DD-69870B7D';
const WIKIMAPIA_CATS = {
    "church": "46,111,122,46511,55296,2790",
    "cafe": "74,108,1083,6121,4668,45467,51109,8274,10835,46468,12992",
    "hotel": "50,30686,43877,1238",
    "store": "17,682,33119,44700,44801,2706,45550",
    "sportstore": "45301,45468",
    "spring": "514,54433,21",
    "clinic": "287,47066,47435,45827,25917"
};

let WIKIMAPIA_CATS_TO_NAMES = {};
for(let k in WIKIMAPIA_CATS) {
    let ids = WIKIMAPIA_CATS[k].split(',');
    for(let id of ids) {
        WIKIMAPIA_CATS_TO_NAMES[id] = k;
    }
}


function hideSingleTrack(id) {
	if(map && infoModel && id in infoModel.tracks) {
		map.removeLayer(infoModel.tracks[id].line);
	}
	return false;
}

function colorByMapiaTag(tags = []) {
    for(let t of tags) {
        const tag = t.id;
        if(tag in WIKIMAPIA_CATS_TO_NAMES) {
            let name = WIKIMAPIA_CATS_TO_NAMES[tag];
            if(name === 'church') return 'darkblue';
            if(name === 'store') return 'orange';
            if(name === 'cafe') return 'green';
            if(name === 'hotel') return 'brown';
            if(name === 'sportstore') return 'cyan';
            if(name === 'spring') return 'blue';
            if(name === 'clinic') return 'red';
        }
    }
    return 'gray';
}

function mkTrackPoly(track, visible) {
	var color = rainbow();
	var l = L.polyline(track.points, {color: color, opacity: DEF_TRACK_OPACITY });
	l.bindPopup(track.id + ': ' + track.title + '<br><a href="" onclick="return hideSingleTrack('+ track.id +')">Скрыть</a>');
	l.on("popupopen", function(e) {
		e.target.bringToFront();
		e.target.setStyle({ opacity: 1 });
	});
	l.on("popupclose", function(e) {
		e.target.setStyle({ opacity: DEF_TRACK_OPACITY });
	});
	if(visible) l.addTo(map);
	// track boundary: L.rectangle(r[i].bbox, {color: color, opacity: 0.5, weight: 3, fill: false }).addTo(map);
	return l;
}

function mkMapiaPoly(mapia, visible) {
    var color = colorByMapiaTag(mapia.tags);
	var l = L.polygon(mapia.polygon, {color: color, opacity: DEF_MAPIA_OPACITY, fillOpacity: 0, weight: 2 });
	//l.bindPopup(mapia.id + ': ' + mapia.title);
	l.on('mouseover', function (e) {
		e.target.setStyle({ opacity: 1, fillOpacity: 0.3 });
		infoModel.currentMapiaObjectTitle(mapia.id + ': ' + mapia.title);
		//this.openPopup();
	});
	l.on('mouseout', function (e) {
		//this.closePopup();
		e.target.setStyle({ opacity: DEF_MAPIA_OPACITY, fillOpacity: 0 });
		infoModel.currentMapiaObjectTitle('');
	});
	l.on('click', function (e) {
		var popupLL = e.target.getBounds().getCenter();
		var popup = L.popup({ maxWidth: 800, autoPan: false }).setLatLng(popupLL).setContent('<img src="/public/ajax-loader.gif">').openOn(map);
		$.get('http://api.wikimapia.org/?function=place.getbyid&language=ru&format=json&key='+ WIKIMAPIA_KEY +'&id='+ mapia.id).then(function(r) {
			popup.setContent(mapia.id + ': ' + mapia.title + '<br>' + r.description);
		});
	});
	if(visible) l.addTo(map);
	return l;
}

function rainbow() {
	var golden_ratio_conjugate = 0.618033988749895;
	
	var r, g, b;
	var h = (Math.random() + golden_ratio_conjugate) % 1;
	var i = ~~(h * 6);
	var f = h * 6 - i;
	var q = 1 - f;
	switch(i % 6){
	case 0: r = 1, g = f, b = 0; break;
	case 1: r = q, g = 1, b = 0; break;
	case 2: r = 0, g = 1, b = f; break;
	case 3: r = 0, g = q, b = 1; break;
	case 4: r = f, g = 0, b = 1; break;
	case 5: r = 1, g = 0, b = q; break;
	}
	var c = "#" + ("00" + (~ ~(r * 255)).toString(16)).slice(-2) + ("00" + (~ ~(g * 255)).toString(16)).slice(-2) + ("00" + (~ ~(b * 255)).toString(16)).slice(-2);
	return (c);
}

function getPlacesFromDocument(doc) {
    let places = [];
    $(doc).find('places').children().each((i,e) => {
        const poly = $(e).find('polygon').children().map((i,e) => [[ parseFloat($(e).find('y').text()), parseFloat($(e).find('x').text()) ]]).get();
        const tags = $(e).find('tags').children().map((i,e) => ({ id: $(e).find('id').text(), title: $(e).find('title').text() })).get();
        places.push({
            id: $(e).find('id').first().text(),
            title: $(e).find('title').first().text(),
            polygon: poly,
            tags: tags
        });
    });
    return places;
}

var InfoModel = function () {
	var self = this;
	self.tracks = {};
	self.mapia = {};
	self.npoints = ko.observable(0);
	self.distance = ko.observable(0);
	self.mapZoom = ko.observable(13);
	self.showTracks = ko.observable(true);
	self.loadTracks = ko.observable(false);
    
	self.showMapia = ko.observable(true);
	self.loadMapia = ko.observable(false);
	self.mapiaCats = ko.observableArray(["church","hotel","cafe","store","sportstore","spring","clinic"]);
    
	self.currentMapiaObjectTitle = ko.observable('');
	self.loadTracks.subscribe(function(val) {
		if(val) self.loadAreaData();
	});
	self.loadMapia.subscribe(function(val) {
		if(val) self.loadAreaData();
	});
	self.showTracks.subscribe(function(val) {
		if(val) {
			for(var k in self.tracks) self.tracks[k].line.addTo(map);
		}
		else {
			for(var k in self.tracks) map.removeLayer(self.tracks[k].line);
		}
	});
	self.showMapia.subscribe(function(val) {
		if(val) {
			for(var k in self.mapia) self.mapia[k].line.addTo(map);
		}
		else {
			for(var k in self.mapia) map.removeLayer(self.mapia[k].line);
		}
	});
	self.clearPath = function() { self.mytrack.removeAllPoints(); };
	self.reversePath = function() { self.mytrack.reversePoints(); };
	self.saveAsGpx = function() {
		var gpxobj = gpxUtil.stringify({ tracks: [ self.mytrack.getLatLngs() ], wpts: self.mytrack.getWpts(), name: self.mytrack.gpxName });
		var blob = new Blob([gpxobj], {type: "text/plain;charset=utf-8"});
		saveAs(blob, "mytrack.gpx");
	};
	self.handleFileChoice = function(m, evt) {
		var file = evt.target.files[0];
		var reader = new FileReader();
		reader.onload =  function(e) {
			if(e.target.readyState != 2) return;
			var gpx = gpxUtil.parse(e.target.result);
			if(gpx.tracks.length == 0) return alert('Not found track in this file');
			var points = gpx.tracks[0];
			self.mytrack.setNewPoints(points, gpx.wpts);
			self.mytrack.gpxName = gpx.name;
			map.fitBounds(self.mytrack.getBounds());
		};
		reader.readAsText(file);
	};
	self.loadAreaData = function() {
		const bnd = map.getBounds();
		const se = bnd.getSouthEast();
		const nw = bnd.getNorthWest();
		const qstr_coords = '&lats=' + se.lat + '&lnge=' + se.lng + '&latn=' + nw.lat + '&lngw=' + nw.lng;
		// map boundary: L.rectangle(bnd, {color: 'white', opacity: 0.5, weight: 3, fill: false }).addTo(map);

        let qstr_categories = self.mapiaCats().map((cat) => WIKIMAPIA_CATS[cat]).join(',');
        if(qstr_categories) qstr_categories = "&category_or=" + qstr_categories;
        const bbox = [nw.lng, se.lat, se.lng, nw.lat].join(',');
	    const mapia_url = `http://api.wikimapia.org/?function=place.getbyarea&bbox=${bbox}${qstr_categories}&format=json&key=${WIKIMAPIA_KEY}&language=ru&count=100&page=1`;
        let rq = [];
        if(self.loadTracks()) rq.push($.get('/tracks?tracks=1' + qstr_coords));
        if(self.loadMapia()) rq.push($.get(mapia_url));
		promise.join(rq).then((result) => {
            let tracks = [], places = [];
			let track_c = 0, mapia_c = 0;
            for(let [r] of result) {
                if('tracks' in r) tracks = r.tracks; // tracks response
                //if('documentElement' in r) places = getPlacesFromDocument(r); // wikimapia response
                if('places' in r) places = r.places; // wikimapia response
            }
			for(let i = 0; i < tracks.length; i++) {
			    let id = tracks[i].id;
			    if(!(id in self.tracks)) {
				    self.tracks[id] = tracks[i];
				    self.tracks[id].line = mkTrackPoly(tracks[i], self.showTracks());
				    track_c++;
			    }
			}
			console.log("loaded " + track_c + " tracks");
			for(let i = 0; i < places.length; i++) {
                const place = places[i];
			    const id = place.id;
			    if(!(id in self.mapia)) {
                    for (let j = 0; j < place.polygon.length; j++) {
                        var p = place.polygon[j];
                        place.polygon[j] = [p.y, p.x];
                    }
				    self.mapia[id] = place;
				    self.mapia[id].line = mkMapiaPoly(place, self.showMapia());
				    mapia_c++;
			    }
			}
			console.log("loaded " + mapia_c + " wikimapia objects");
		});
	};
};

var infoModel = new InfoModel();
ko.applyBindings(infoModel, document.getElementById('info'));

var startCoords = [55.751726, 37.619087];


var baseLayers = {};
var overlayLayers = {};

baseLayers['Yandex'] = new L.Yandex();
baseLayers['Yandex Hybrid'] = new L.Yandex('hybrid');
baseLayers['Yandex Sat'] = new L.Yandex('satellite');
baseLayers['Google'] = new L.Google('ROADMAP');
baseLayers['Google Sat'] = new L.Google();
baseLayers['Google Hybrid'] = new L.Google('HYBRID');
baseLayers['Google Terrain'] = new L.Google('TERRAIN');

baseLayers['OSM'] = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
});

baseLayers['Esri WorldImagery'] = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
	attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
});

baseLayers['Hike Bike'] = L.tileLayer('http://{s}.tiles.wmflabs.org/hikebike/{z}/{x}/{y}.png', {
	attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
});

baseLayers['MTB map'] = L.tileLayer('http://tile.mtbmap.cz/mtbmap_tiles/{z}/{x}/{y}.png', {
	attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &amp; USGS'
});

baseLayers['Wikimapia'] = L.tileLayer("http://i{ss}.wikimapia.org/?x={x}&y={y}&zoom={z}", {
	attribution: "Maps from <a href=\"http://wikimapia.org/\">Wikimapia.org</a> and contributors <a href=\"http://creativecommons.org/licenses/by-nc-sa/3.0/\">CC-BY-NC-SA</a>",
	maxZoom:21,
	ss: function(d) { return d.x%4 + (d.y%4)*4; }
})

var map = L.map('map', {
	center: startCoords,
	zoom: infoModel.mapZoom(),
	layers: [ baseLayers['Yandex'] ]
});
map.on('zoomend', function(e) {
	infoModel.mapZoom(e.target.getZoom());
});
map.on('moveend', function(e) {
	infoModel.loadAreaData();
});

L.control.layers(baseLayers, overlayLayers).addTo(map);


// path draw
infoModel.mytrack = L.Polyline.Plotter([], {color: 'red'}).addTo(map);

infoModel.pathAltered = function(e) {
	var popupLL = infoModel.mytrack.getLatLngs();
	var dist = 0.0;
	for(var i = 0; i < popupLL.length; i++) {
		if(i > 0) dist += popupLL[i].distanceTo(popupLL[i-1]);
	}
	infoModel.npoints(popupLL.length);
	infoModel.distance(L.Util.formatNum(dist,-1) + ' м');
}

infoModel.mytrack.on('plotter.alter', infoModel.pathAltered);
infoModel.mytrack.on('plotter.add', infoModel.pathAltered);
infoModel.mytrack.on('plotter.rm', infoModel.pathAltered);
