package main

import (
	"compress/gzip"
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/coopernurse/gorp"
	"github.com/gorilla/context"
	"github.com/julienschmidt/httprouter"
	_ "github.com/lib/pq"
	_ "github.com/mattn/go-sqlite3"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"
)

const DB_FILE = "/tmp/gps1.bin"
const WIKIMAPIA_KEY = "41F4981A-1BD068F3-770D849B-345A4C4B-496E3A04-CF0F0ABF-44F459DD-69870B7D"

var (
	Dbh *gorp.DbMap
)

type Track struct {
	Id       int64         `json:"id"`
	Title    string        `json:"title"`
	GpsiesId string        `json:"-"`
	Bbox     [2][2]float64 `json:"bbox"`
	Points   [][2]float64  `json:"points"`
}

/*
type TrackPoint struct {
	Id      int64   `json:"-"`
	Lat     float64 `json:"a"`
	Lng     float64 `json:"n"`
	Height  float64 `json:"-"`
	TrackId int64   `json:"-"`
}
*/

type TrackRaw struct {
	Id       int64
	Title    string
	Data     string
	Lngw     float64
	Latn     float64
	Lnge     float64
	Lats     float64
	GpsiesId string
}

type AreaAjaxResp struct {
	Tracks []Track        `json:"tracks"`
	Mapia  []WmapiaPlace1 `json:"mapia"`
}

type WmapiaPlace1 struct {
	Id     int64        `json:"id"`
	Title  string       `json:"title"`
	Type   string       `json:"type"`
	Points [][2]float64 `json:"points"`
}

type WmapiaResp struct {
	Found  string        `json:"found"`
	Places []WmapiaPlace `json:"places"`
}

type WmapiaPlace struct {
	Id      int64         `json:"id"`
	Title   string        `json:"title"`
	Polygon []WmapiaCoord `json:"polygon"`
	Tags    []WmapiaTag   `json:"tags"`
}

type WmapiaTag struct {
	Id    int64  `json:"id"`
	Title string `json:"title"`
}

type WmapiaCoord struct {
	X float64 `json:"x"`
	Y float64 `json:"y"`
}

type gzipResponseWriter struct {
	io.Writer
	http.ResponseWriter
}

func (w gzipResponseWriter) Write(b []byte) (int, error) {
	return w.Writer.Write(b)
}

func makeGzipHandler(fn httprouter.Handle) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, pr httprouter.Params) {
		if !strings.Contains(r.Header.Get("Accept-Encoding"), "gzip") {
			fn(w, r, pr)
			return
		}
		w.Header().Set("Content-Encoding", "gzip")
		gz := gzip.NewWriter(w)
		defer gz.Close()
		gzr := gzipResponseWriter{Writer: gz, ResponseWriter: w}
		fn(gzr, r, pr)
	}
}

func Index(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fmt.Fprint(w, "Welcome!\n")
}

func GetAreaData(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	coordBinds := map[string]string{"lngw": "", "latn": "", "lnge": "", "lats": ""}
	for k := range coordBinds {
		coordBinds[k] = r.URL.Query().Get(k)
	}
	resp := AreaAjaxResp{Tracks: []Track{}, Mapia: []WmapiaPlace1{}}
	var err1, err2 error
	if r.URL.Query().Get("tracks") != "" {
		resp.Tracks, err1 = loadTracksByArea(coordBinds)
	}
	if r.URL.Query().Get("mapia") != "" {
		resp.Mapia, err2 = getWikimapiaData(coordBinds)
	}

	if !notifyError(w, err1) && !notifyError(w, err2) {
		genericJsonResponse(w, resp)
	}
}

func loadTracksByArea(coordBinds map[string]string) ([]Track, error) {
	var trackRows []TrackRaw
	start := time.Now()
	_, err := Dbh.Select(&trackRows, "SELECT * FROM tracks WHERE Lngw < :lnge AND Latn > :lats AND Lnge > :lngw AND Lats < :latn", coordBinds)
	if err != nil {
		return nil, err
	}
	//_, err1 := Dbh.Select(&trackRows, "SELECT * FROM tracks WHERE Id = 29 LIMIT 30", coordBinds)
	tracks := make([]Track, len(trackRows))
	log.Printf("select took: %s, rows: %d\n", time.Since(start), len(trackRows))
	start = time.Now()
	var wg sync.WaitGroup
	wg.Add(len(trackRows))
	for i, _ := range trackRows {
		go func(i int) {
			tracks[i] = *parsePltTrack(trackRows[i].Data)
			tracks[i].Id = trackRows[i].Id
			tracks[i].Bbox = [2][2]float64{[2]float64{trackRows[i].Lats, trackRows[i].Lngw}, [2]float64{trackRows[i].Latn, trackRows[i].Lnge}}
			wg.Done()
		}(i)
	}
	wg.Wait()
	log.Printf("parse took: %s\n", time.Since(start))
	return tracks, nil
}

func getWikimapiaData(coordBinds map[string]string) ([]WmapiaPlace1, error) {
	// lon_min,lat_min,lon_max,lat_max
	bbox := strings.Join([]string{coordBinds["lngw"], coordBinds["lats"], coordBinds["lnge"], coordBinds["latn"]}, ",")
	//bbox := "37.64237880706787,55.72471746517276,37.71387577056885,55.744168356179514"
	url := "http://api.wikimapia.org/?function=place.getbyarea&bbox=" + bbox + "&key=" + WIKIMAPIA_KEY + "&format=json&language=ru&count=100&page=1"
	res, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	var data WmapiaResp
	err = json.Unmarshal(body, &data)
	if err != nil {
		return nil, err
	}
	//log.Printf("tgs: %v\n", data.Places)
	result := make([]WmapiaPlace1, 0, len(data.Places))
	for i := range data.Places {
		p := data.Places[i]
		ptype, ptypen := "", int64(0)
		for x := range p.Tags {
			if p.Tags[x].Id < ptypen || ptypen == 0 {
				ptype = p.Tags[x].Title
				ptypen = p.Tags[x].Id
			}
		}
		result = append(result, WmapiaPlace1{Id: p.Id, Title: p.Title, Type: ptype, Points: make([][2]float64, len(p.Polygon))})
		for j := range p.Polygon {
			result[i].Points[j] = [2]float64{p.Polygon[j].Y, p.Polygon[j].X}
		}
	}
	//fmt.Printf("Results: %v\n", result)
	return result, nil
}

func parsePltTrack(data string) *Track {
	lines := strings.Split(data, "\r\n")
	track := &Track{Points: make([][2]float64, 0, len(lines)-7)}
	for c, l := range lines {
		if c == 4 {
			cols := strings.Split(l, ",")
			track.Title = cols[3]
		}
		if c < 6 {
			continue
		}
		cols := strings.Split(l, ", ")
		if len(cols) > 3 {
			lat, _ := strconv.ParseFloat(cols[0], 64)
			lng, _ := strconv.ParseFloat(cols[1], 64)
			//height, _ := strconv.ParseFloat(cols[3], 64)
			track.Points = append(track.Points, [...]float64{lat, lng}) // TrackPoint{Lat: lat, Lng: lng, Height: height}
		}
	}
	return track
}

func initDB(dbFile string) *gorp.DbMap {
	var dbmap *gorp.DbMap

	dbHost := os.Getenv("OPENSHIFT_POSTGRESQL_DB_HOST")
	dbPort := os.Getenv("OPENSHIFT_POSTGRESQL_DB_PORT")
	dbUser := os.Getenv("OPENSHIFT_POSTGRESQL_DB_USERNAME")
	dbPass := os.Getenv("OPENSHIFT_POSTGRESQL_DB_PASSWORD")

	if dbHost != "" {
		// openshift
		connection := fmt.Sprintf("postgres://%s:%s@%s:%s?sslmode=disable", dbUser, dbPass, dbHost, dbPort)
		log.Println(connection)
		db, err := sql.Open("postgres", connection)
		errFatal(err, "sql.Open failed")
		dbmap = &gorp.DbMap{Db: db, Dialect: gorp.PostgresDialect{}}
	} else if connection := os.Getenv("DATABASE_URL"); connection != "" {
		// heroku
		db, err := sql.Open("postgres", connection)
		errFatal(err, "sql.Open failed")
		dbmap = &gorp.DbMap{Db: db, Dialect: gorp.PostgresDialect{}}
	} else {
		// local
		db, err := sql.Open("sqlite3", dbFile)
		errFatal(err, "sql.Open failed")
		dbmap = &gorp.DbMap{Db: db, Dialect: gorp.SqliteDialect{}}
		dbmap.TraceOn("[gorp]", log.New(os.Stdout, "myapp:", log.Lmicroseconds))
	}

	// register tables
	dbmap.AddTableWithName(TrackRaw{}, "tracks").SetKeys(true, "Id")
	//err = dbmap.CreateTablesIfNotExists()
	//errFatal(err, "Create tables failed")
	return dbmap
}

func main() {
	nCPU := runtime.NumCPU()
	log.Printf("host ncpus:%d\n", nCPU)
	runtime.GOMAXPROCS(nCPU)

	Dbh = initDB(DB_FILE)
	router := httprouter.New()
	router.ServeFiles("/public/*filepath", http.Dir("./public"))
	router.GET("/", Index)
	router.GET("/tracks", makeGzipHandler(GetAreaData))

	bind := fmt.Sprintf("%s:%s", os.Getenv("HOST"), os.Getenv("PORT"))
	log.Fatal(http.ListenAndServe(bind, context.ClearHandler(router)))
}

func errFatal(err error, msg string) {
	if err != nil {
		log.Fatalln(msg, err)
	}
}

func notifyError(w http.ResponseWriter, err error) bool {
	if err == nil {
		return false
	} else {
		_, file, line, _ := runtime.Caller(1)
		log.Printf("%s:%d %s\n", file, line, err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return true
	}
}

func genericJsonResponse(w http.ResponseWriter, v interface{}) {
	jsonBytes, err := json.Marshal(v)
	//log.Println(string(jsonBytes))
	if notifyError(w, err) {
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonBytes)
}
